# Blog builder

A tool to generate static blog pages using a combination of YAML and Markdown/HTML.

## Installing

Simply clone the Git repository

```
$ git clone https://gitlab.com/choufani/blog-builder
```

 or add it as a submodule to your project.

 ```
 $ git submodule add https://gitlab.com/choufani/blog-builder
 $ git submodule update --init
 ```

## Creating blog

First add a site configuration YAML file base on [site.yml.example](./site.yml.example).

Add posts to your configured posts directory based on [post.yml.example](./post.yml.example). Content can be added using HTML or Markdown.

Add master page template to be used to generate posts (check [master.html](./master.html) for inspiration). You may use all posts configuration directives, `site.title` and `site.license` directives in your master template file in addition to the special navigation variables `nav.prev`, `nav.next`, `nav.home` and `nav.permalink` to be replaced by corresponding navigation links relative to the configured site root.

Run the build script with your site configuration file as a parameter to generate the pages into the configured output directory.
```
$ ./builder.py mysite.yml
```
