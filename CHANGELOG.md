# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

[//]: # (Maintainer comments are added using this line's format.)

## [0.4.0] - 2017-04-03
### Added
- Temporary functions representing built-in "modules" behaviour for site/post/navigation data.

### Changed
- Post data generation via new temporary "modules" functions

### Deprecated
- Template update function `buildlib.update_template`

## [0.3.1] - 2017-04-02
### Added
- Handling of post note and summary directives

## [0.3.0] - 2017-04-01
### Added
- Aggregate build workflow function.
- Introduced `buildlib.VERSION_CODE` constant for easy version check.

### Changed
- Moved build code into separate `buildlib` library module.

### Fixed
- Sphinx + napoleon Google Style Python docstrings.

## [0.2.2] - 2017-03-31
### Changed
- Next and previous links are relative (independent of configured root).

## [0.2.1] - 2017-03-31
### Fixed
- Removed forgotten testing print statement.

## [0.2.0] - 2017-03-31
### Added
- Site root configuration.
- Permalink `nav.permalink` and home `nav.home` navigation links variables.

### Changed
- Previous and next navigation links relative to site root.

## [0.1.3] - 2017-03-31
### Added
- WTFPL license file.

## [0.1.2] - 2017-03-31
### Added
- Sample files.

## [0.1.1] - 2017-03-31
### Added
- Missing requirements.txt PyPy requirements file.

## [0.1.0] - 2017-03-31
### Added
- Build script from blog project.
