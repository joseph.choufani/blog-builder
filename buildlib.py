# -*- coding: utf-8 -*-
"""Build library.
"""

__all__ = [
    'BuildError', 'ConfigurationError', 'Configuration', 'MasterFileError',
    'PostDirError', 'PostConfigError', 'NoPostsError',
]

import os
import codecs
#  PyYAML and Markdown package are external dependencies. PyLint might produce
#> an `import-error` if packages are installed into virtualenv.
# pylint: disable=e0401,import-error
import yaml
import markdown
# pylint: enable=e0401,import-error

VERSION_CODE = 3
"""int: Incremental integer version code, helpful for version comparison.

Example:
    ```
    if getattr(buildlib, 'VERSION_CODE', 0) >= MIN_VERSION:
        # Use feature
        ...
    else:
        # Do something else
        ...
    ```

"""

SITE_TITLE = 'site.title' # Blog title configuration directive
SITE_TEMP = 'site.master' # Master template file configuration directive
SITE_POSTDIR = 'site.posts' # Posts directory configuration directive
SITE_OUTDIR = 'site.output' # Output directiry configuration directive
SITE_ANON = 'site.anon_prefix' # Untitled prefix configuration directive
SITE_LIC = 'site.license' # License configuration directive
SITE_NAVFMT = 'site.navformat' # Navigation link configuration directive
SITE_ROOT = 'site.root' # Hyperlink root configuration directives
"""str: SITE_* blog configuration directives constants.
"""

POST_TITLE = 'post.title' # Post title configuration directive
POST_DATE = 'post.date' # Post date configuration directive
POST_CONTENT = 'post.content' # Post content configuration directive
POST_NAME = 'post.name' # Post HTML file name configuration directive
POST_AUTH = 'post.author' # Post author configuration directive
POST_NOTE = 'post.note' # Post note configuration directive
POST_TLDR = 'post.summary' # Post summary configuration directive
"""str: POST_* blog post configuration directives constants.
"""

DEF_POSTDIR, DEF_OUTDIR, DEF_ANON, DEF_LIC, DEF_NAVFMT, DEF_AUTH, DEF_ROOT = \
    'posts', 'public', 'post', '', '<a href="{link}">{title}</a>', 'Author', ''
"""str: Default values for optional blog configurations.
"""

NAV_PREV, NAV_NEXT, NAV_LINK, NAV_HOME = \
    'nav.prev', 'nav.next', 'nav.permalink', 'nav.home'
"""str: Navigation data directives.
"""

class BuildError(Exception):
    """Base class for all build exceptions.
    """
    pass

class ConfigurationError(BuildError):
    """Alias for error raised during configuration.
    """
    pass
class Configuration(object):
    """YAML configuration wrapper and dictionary wrapper.

    Items are accessed like dictionary items.

    Attributes:
        dirname (str): Path to directory containing configuration file.

    Raises:
        :obj:`ConfigurationError`: Error reading configuration file.

    """
    def __init__(self, conf_path):
        try:
            self.__config = \
                yaml.load(codecs.open(conf_path, encoding="utf-8").read())
            self.__dir = os.path.dirname(conf_path)
        except (IOError, UnicodeError, yaml.YAMLError):
            raise ConfigurationError
    def __getitem__(self, key):
        return self.__config[key]
    def get(self, key, default=None):
        """Wrapper for internal dictionary's get method."""
        return self.__config.get(key, default)
    @property
    def dirname(self):
        """."""
        return self.__dir

class MasterFileError(BuildError):
    """Alias for error raised during master file reading.
    """
    pass
def read_master(config):
    """Function that reads configured master template file.

    Args:
        :obj:`Configuration`: Configuration object.

    Raises:
        :obj:`MasterFileError`: Error reading master template file.

    """
    try:
        path = os.path.join(config.dirname, config[SITE_TEMP])
        return codecs.open(path, encoding="utf-8").read()
    except (KeyError, IOError, UnicodeError):
        raise MasterFileError

class PostDirError(BuildError):
    """Alias for error raised during reading directory containing posts.
    """
    pass
class PostConfigError(BuildError):
    """Alias of errors raise during post configuration reading.

    Args:
        post_name (str): Post identifier string.

    Attributes:
        post (str): Post identifier string.

    """
    def __init__(self, post_name):
        self.post = post_name
        super(PostConfigError, self).__init__()
class NoPostsError(BuildError):
    """Alias of error raised when no posts are found.
    """
    pass
def get_posts(config):
    """Function that gets a sorted list of YAML posts configurations
    dictionaries.

    Args:
        :obj:`Configuration`: Configuration object.

    Returns:
        :obj:`list` of :obj:`dict`:
            Sorted list of YAML posts configurations dictionaries.

    Raises:
        :obj:`PostDirError`
        :obj:`PostConfigError`
        :obj:`NoPostsError`

    """
    try:
        path = \
            os.path.join(config.dirname, config.get(SITE_POSTDIR, DEF_POSTDIR))
        posts_list = []
        for post in os.listdir(path):
            post_config = yaml.load(
                codecs.open(os.path.join(path, post), encoding="utf-8").read())
            #  Each post is expected to have a title, a date and content
            if post_config.get(POST_TITLE) and post_config.get(POST_DATE) \
               and post_config.get(POST_CONTENT):
                posts_list.append(post_config)
            else:
                raise ValueError
        if not posts_list:
            raise NoPostsError
        #  Returns sorted list by date
        posts_list.sort(key=lambda o: o[POST_DATE])
        return posts_list
    except (OSError,):
        raise PostDirError
    except (ValueError, AttributeError, IOError, UnicodeError, yaml.YAMLError):
        raise PostConfigError(post)

def site_module(_posts, _idx, config):
    """Temporary function that return site data for posts.
    """
    return {
        SITE_TITLE:config[SITE_TITLE],
        SITE_LIC:config.get(SITE_LIC, DEF_LIC)
    }
def post_module(posts, idx, _config):
    """Temporary function that returns a post's data.
    """
    data = posts[idx].copy()
    data[POST_NOTE] = data.get(POST_NOTE, '')
    data[POST_TLDR] = data.get(POST_TLDR, '')
    data[POST_CONTENT] = \
        markdown.markdown(data[POST_CONTENT], output_format='html')
    data[POST_AUTH] = data.get(POST_AUTH, DEF_AUTH) or DEF_AUTH
    return data
def nav_module(posts, idx, config):
    """Temporary function that returns a post's navigation data.
    """
    post = posts[idx].copy()
    lnk_fmt = config.get(SITE_NAVFMT, DEF_NAVFMT)
    root = config.get(SITE_ROOT, DEF_ROOT)
    prefix = config.get(SITE_ANON, DEF_ANON)
    post[NAV_PREV] = lnk_fmt.format(
        title="Previous",
        link="{}.html".format(
            posts[idx - 1].get(POST_NAME, "{}{}".format(prefix, idx)))) \
        if idx > 0 else ''
    post[NAV_NEXT] = lnk_fmt.format(
        title="Next",
        link="{}.html".format(
            posts[idx + 1].get(
                POST_NAME, "{}{}".format(prefix, idx + 2)))) \
        if idx < len(posts) - 1 else ''
    post[NAV_HOME] = os.path.join(root, 'index.html')
    page = "{}.html".format(
        post.get(POST_NAME, "{}{}".format(prefix, idx + 1)))
    post[NAV_LINK] = os.path.join(root, page)
    return post

def write_posts(temp, config, posts):
    """Function that writes posts into configured output directory.

    Args:
        temp (str): Post template.
        config (:obj:`Configuration`): Site configuration object.
        posts (:obj:`list` of :obj:`dict`):
            List of posts as posts configuration dictionaries.

    Raises:
        :obj:`OSError`: Failed to created output directory.
        :obj:`IOError`: Failed to write to output directory.

    """
    path = os.path.join(config.dirname, config.get(SITE_OUTDIR, DEF_OUTDIR))
    if not os.path.lexists(path):
        os.mkdir(path)
    prefix = config.get(SITE_ANON, DEF_ANON)
    for i, post in enumerate(posts):
        #  Site module
        post.update(site_module(posts, i, config))
        #  Post module
        post.update(post_module(posts, i, config))
        #  Navigation module
        post.update(nav_module(posts, i, config))
        #  Updates template data
        post_page = temp
        for k in post:
            post_page = post_page.replace("[%s]" % k, post[k])
        #  Writes page
        page = "{}.html".format(
            post.get(POST_NAME, "{}{}".format(prefix, i + 1)))
        with codecs.open(
            os.path.join(path, page), mode='wb', encoding="utf-8") as post_fd:
            post_fd.write(post_page)
        #  Writes latest post to index.html
        if post.get(NAV_NEXT) == '':
            with codecs.open(
                os.path.join(path, 'index.html'), mode='wb', encoding="utf-8")\
                as index_fd:
                index_fd.write(post_page)

def build(config_path):
    """Aggregate build workflow function.

    Args:
        config_path (str): Path to configuration file.

    Raises:
        :obj:`ConfigurationError`: Configuration file error.
        :obj:`MasterFileError`: Failure to read master file.
        :obj:`PostDirError`: Failure to read posts directory.
        :obj:`NoPostsError`: Posts directory is empty.
        :obj:`PostConfigError`: Failure to read a post's configuration.
        :obj:`OSError` or :obj:`IOError`: Failure to write output.

    """
    #  Reads site config file
    config = Configuration(config_path)
    #  Reads configured master file
    master = read_master(config)
    #  Creates list of posts
    posts_list = get_posts(config)
    #  Generates posts in output directory
    write_posts(master, config, posts_list)
