#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Build script.
"""

import argparse
import os
import sys
import buildlib

def resolve_path(path):
    """Function that resolves the full path of a given path following symlinks.

    Args:
        path (str): Path to resolve.

    Returns:
        str: Resolved full path.

    Usage:
        ```
        $ MYDIR='my_dir' python
        >>> from build import resolve_path
        >>> resolve_path('~/$MYDIR')
        '/home/user/my_dir'
        ```

    """
    return \
        os.path.realpath(
            os.path.abspath(
                os.path.expanduser(
                    os.path.expandvars(path))))

def main():
    """Main build function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config', type=resolve_path, help='site config YAML file')
    args = parser.parse_args()
    try:
        code = 0 # Initialises return code to 0=no-error
        buildlib.build(args.config)
    except buildlib.ConfigurationError:
        code = 1
        sys.stderr.write("Failed to read site config file %s.\n" % args.config)
    except buildlib.MasterFileError:
        code = 10
        sys.stderr.write("Failed to read template file.\n")
    except buildlib.PostDirError:
        code = 11
        sys.stderr.write("Failed to get list of posts.\n")
    except buildlib.NoPostsError:
        code = 12
        sys.stderr.write("Could not find any posts.\n")
    except buildlib.PostConfigError as err:
        code = 13
        sys.stderr.write("Failed to read post %s.\n" % err.post)
    except (OSError, IOError):
        code = 14
        sys.stderr.write("Failed to write output.\n")
    finally:
        sys.exit(code)

if __name__ == '__main__':
    main()
